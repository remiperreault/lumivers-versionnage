﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupRotation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //rotation de l'item sur lui-même
        transform.Rotate(new Vector3(25, 40, 55) * Time.deltaTime);
    }
}
