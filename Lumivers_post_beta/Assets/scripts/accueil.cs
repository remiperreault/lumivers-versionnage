﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class accueil : MonoBehaviour
{


    public float delaiAnimIntroJeu = 5f;

[Header("Video Players")]
    public GameObject vp_animationIntroJeu;

 void Start() {
    vp_animationIntroJeu.SetActive(false);
}

    // Update is called once per frame
    void Update()
    {
        //écouteur sur les touches clavier. Réagit quand une des touche suivante est pesée: 5, numpad 5, barre d'espace
        if (Input.GetKeyDown("5") || Input.GetKeyDown("[5]") || Input.GetKeyDown("space"))
        {

            //affiche l'animation
            vp_animationIntroJeu.SetActive(true);

             //delai avant changement de monde
                StartCoroutine(LoadScene("monde1", delaiAnimIntroJeu));

        }
    }

IEnumerator LoadScene(string sceneToLoad, float loadDelay)
    {
        yield return new WaitForSeconds(loadDelay);

        //chargement du monde correspondant au code utilisé par le joueur
        SceneManager.LoadScene(sceneToLoad);

    }


}
