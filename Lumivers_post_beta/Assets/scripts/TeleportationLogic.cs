﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TeleportationLogic : MonoBehaviour
{
    //pour le verouillage des commandes a la fin du jeu
    public bool partieEnCours;

    //jauge de temps
    public GameObject jaugeTemps;
    public Transform timeBar;
    static float timeBarWidth = 1;

    //mettre la même valeur aux deux variables //240 pour une session de 4 minutes
    static float countdown = 300;

    public float maxTempsDeJeu = 300;

    //entrée de code
    public Text typedCodeText;
    public string inputCode;
    public string typedCode;



    //public string unlockedCodeFragments;

    [Header("Textes fragments monde1")]
    public Text pickedUpCodesFragment1monde1Text;
    public Text pickedUpCodesFragment2monde1Text;
    public Text pickedUpCodesFragment3monde1Text;

    [Header("Textes fragments monde2")]
    public Text pickedUpCodesFragment1monde2Text;
    public Text pickedUpCodesFragment2monde2Text;
    public Text pickedUpCodesFragment3monde2Text;

    [Header("Textes fragments monde3")]
    public Text pickedUpCodesFragment1monde3Text;
    public Text pickedUpCodesFragment2monde3Text;
    public Text pickedUpCodesFragment3monde3Text;


    public bool codeMonde1Trouve;
    public bool codeMonde2Trouve;
    public bool codeMonde3Trouve;

    public string world1Code = "701";
    public string world2Code = "824";
    public string world3Code = "593";

    [Header("Video Duration")]
    public float videoDuration = 5f;
    public float videoFinalDuration = 15f;

    [Header("Video Players")]
    public GameObject vp_animationDebutMonde2;//2
    public GameObject vp_animationDebutMonde3;//3
    public GameObject vp_animationDebutMonde1;//1

    public GameObject vp_animationFinJeu;

    [Header("Camera")]

    public Camera camera;
    AudioSource musicAudioSource;




    // Start is called before the first frame update
    void Start()
    {


        //on récupère la source audio de la caméra
        musicAudioSource = camera.GetComponent<AudioSource>();

        //on redonne le controle du clavier au joueur
        partieEnCours = true;

        //on affiche la barre de temps
        jaugeTemps.SetActive(true);


        vp_animationDebutMonde2.SetActive(false);
        vp_animationDebutMonde3.SetActive(false);
        vp_animationDebutMonde1.SetActive(false);

        vp_animationFinJeu.SetActive(false);

    }





    void ClearTypedCode()
    {
        //la saisie est supprimee apres 5 secondes
        typedCode = "";
        typedCodeText.text = "";
    }





    // Update is called once per frame
    void Update()
    {
        //si il reste du temps, le joueur peu utiliser le clavier et changer de monde 
        if (partieEnCours == true)
        {


            //timer logic
            countdown -= Time.deltaTime;

            if (countdown <= 0)
            {
                countdown = 0;
            }

            if (countdown <= 0) //Trigger fin du jeu: animation finale +  Téléportation vers l'écran d'accueil (scène avec le logo animé)
            {

                musicAudioSource.mute = true;

                //on cache la barre de temps (qui est vide)
                jaugeTemps.SetActive(false);


                //jouer l'animation de fin de partie
                TeleportationAnimation(vp_animationFinJeu, "Fin");

                //Réinitialisation du jeu pour la prochaine session (pour le prochain joueur)
                GameReset();

                //chargement du monde correspondant au code utilisé par le joueur
                StartCoroutine(LoadScene("accueil", videoFinalDuration));

                // verouillage des commandes a la fin du jeu
                partieEnCours = false;


            }

            //timeBar logic
            //fraction demps rstant sur temps total
            timeBarWidth = countdown / maxTempsDeJeu;

            //on rétrécit la barre de temps // 1f est la valeur de base, pour aucun changement d'échelle
            timeBar.localScale = new Vector3(1f, timeBarWidth);



            //enregistre le chiffre choisi
            if (Input.GetKeyDown("0") || Input.GetKeyDown("[0]"))
            {
                //assigne le chiffre pese a la variable inputCode
                inputCode = "0";
                //appelle la fonction qui actualise la combinaison entree et son texte dans l'interface
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("1") || Input.GetKeyDown("[1]"))
            {
                inputCode = "1";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("2") || Input.GetKeyDown("[2]"))
            {
                inputCode = "2";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("3") || Input.GetKeyDown("[3]"))
            {
                inputCode = "3";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("4") || Input.GetKeyDown("[4]"))
            {

                inputCode = "4";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("5") || Input.GetKeyDown("[5]"))
            {
                inputCode = "5";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("6") || Input.GetKeyDown("[6]"))
            {
                inputCode = "6";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("7") || Input.GetKeyDown("[7]"))
            {
                inputCode = "7";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("8") || Input.GetKeyDown("[8]"))
            {
                inputCode = "8";
                SetTypedCodeText();
            }
            else if (Input.GetKeyDown("9") || Input.GetKeyDown("[9]"))
            {
                inputCode = "9";
                SetTypedCodeText();
            }


            //effacage de la saisie de code
            if (Input.GetKeyDown("0") || Input.GetKeyDown("[0]"))
            {
                inputCode = "0";
                ClearTypedCode();
            }

        }
    }

    void SetTypedCodeText()
    {
        //quel code est deja trouve
        if (pickedUpCodesFragment1monde1Text.text + pickedUpCodesFragment2monde1Text.text + pickedUpCodesFragment3monde1Text.text == world2Code)
        {
            codeMonde2Trouve = true;
        }

        if (pickedUpCodesFragment1monde2Text.text + pickedUpCodesFragment2monde2Text.text + pickedUpCodesFragment3monde2Text.text == world3Code)
        {
            codeMonde3Trouve = true;
        }

        if (pickedUpCodesFragment1monde3Text.text + pickedUpCodesFragment2monde3Text.text + pickedUpCodesFragment3monde3Text.text == world1Code)
        {
            codeMonde1Trouve = true;
        }

        //limite le code a 3 chiffres
        if (typedCode.Length < 3)
        {
            //on ajoute le dernier chiffe entre a la combinaison
            typedCode += inputCode;

            //affiche la combinaison entree
            typedCodeText.text = typedCode;



            //assemblage du code récuperé
            //unlockedCodeFragments = pickedUpCodesFragment1Text.text + pickedUpCodesFragment2Text.text + pickedUpCodesFragment3Text.text;

            //vérifier si la combinaison est bonne et que les fragments ont tous été récupérés 
            // if (typedCode == world2Code && unlockedCodeFragments == world2Code)

            if (typedCode == world2Code && codeMonde2Trouve)

            {

                TeleportationAnimation(vp_animationDebutMonde2, "Transfert");

                //chargement du monde correspondant au code utilisé par le joueur

                StartCoroutine(LoadScene("monde2", videoDuration));

            }

            if (typedCode == world3Code && codeMonde3Trouve)

            {


                TeleportationAnimation(vp_animationDebutMonde3, "Transfert");

                //chargement du monde correspondant au code utilisé par le joueur

                StartCoroutine(LoadScene("monde3", videoDuration));
            }

            if (typedCode == world1Code && codeMonde1Trouve)

            {
                TeleportationAnimation(vp_animationDebutMonde1, "Transfert");

                //chargement du monde correspondant au code utilisé par le joueur

                StartCoroutine(LoadScene("monde1", videoDuration));
            }

        }




    }

    void TeleportationAnimation(GameObject quelVideoPlayer, string message)
    {



        typedCodeText.text = message;

        //activation de l'animation (video)
        quelVideoPlayer.SetActive(true);

        //on arrete d'effacer le texte
        CancelInvoke("ClearTypedCode");

    }

    //preparation pour la prochaine session de jeu
    void GameReset()
    {



        //on rétrécit la barre de temps à zéro pendant l'animation de fin de aprtie 
        //timeBar.localScale = new Vector3(1f, 0);

        //on efface les codes sauvegardes par le joueur
        codeMonde1Trouve = false;
        codeMonde2Trouve = false;
        codeMonde3Trouve = false;


        //appelle la fonction de l'autre script, qui réinitialise les fragments de code collectés
        gameObject.GetComponent<PlayerWorldInteractions>().ResetCollectedCodeFragments();

        //on remet le compteur de temps a zero
        countdown = maxTempsDeJeu;
    }

    IEnumerator LoadScene(string sceneToLoad, float loadDelay)
    {
        yield return new WaitForSeconds(loadDelay);

        //chargement du monde correspondant au code utilisé par le joueur
        SceneManager.LoadScene(sceneToLoad);

    }





}
