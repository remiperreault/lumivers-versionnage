﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Uduino;

public class UrduinoInput : MonoBehaviour
{
    //******************************1h in**********************************************************

    public int sensorValue;
    public float previous_timeStamp = 0;
    public float tempsEntreAimants; // temps écoulé entre le passage des deux derniers aimants
    public bool dejaEnContact = false;
    public float tempsEcoule;//compteur de temps, horloge
    public float tempsSansMouvement=10;//pour que le vaisseau soit immobile au debut du jeu

    // Start is called before the first frame update
    void Start()
    {
        //référence le capteur magnétique du arguino à un UrduinoManager (connextion et interaction à Unity)
        UduinoManager.Instance.pinMode(2, PinMode.Input_pullup);

        //logique de rotation de la roue
        //previous_timeStamp = Time.deltaTime;
        tempsEntreAimants = 0;
    }

    // Update is called once per frame
    void Update()
    {
      sensorValue = UduinoManager.Instance.digitalRead(2);//enregistrer la valeur du capteur dans une variable (0 ou 1)

        tempsEcoule += Time.deltaTime;
        tempsSansMouvement += Time.deltaTime;

        //si un aimant est détecté par le senseur (si la valeur passe à 0)
        if (sensorValue == 0)
        {

            
            
            if (!dejaEnContact)//si la roue n'est pas simplement immobile, un aimant sur le capteur
            {

                //reset float tempsSansMouvement/*********************************
                tempsSansMouvement = 0;

                /* on indique qu'un contact a été fait, pour s'assurer que le calcul de l'intervalle ne prenne en compte
                le passage d'un aimant une seule fois (et non a chaque frame que l'aimant est au-dessus du sensor)  */
                dejaEnContact = true;

                //si aucun aimant n'a été capté avant
                if (previous_timeStamp == 0) 
                {
                    // on vient de détecter le premier aimant, donc il n'y a pas d'intervalle
                    tempsEntreAimants = 0;
                    //on enregistre le temps actuel comme dernier moment de contact entre aimant et capteur
                    previous_timeStamp = tempsEcoule;
                }
                else// un aimant a deja ete detecte avant, on peut calculer l'intervalle de temps (vitesse de rotation)
                {

                    // on obtient le temps écoulé depuis le passage du dernier aimant = temps actuel - temps lors du passage du dernier aimant
                    tempsEntreAimants = tempsEcoule - previous_timeStamp;

                    //on enregistre le moment du passage de l'aimant, pour pouvoir le comparer avec le prochain et calculer la vitesse de rotation du prochain cycle
                    previous_timeStamp = tempsEcoule;

                }
                                                                             
            }
       
        }
        else if(sensorValue == 1)//si le capteur magnétique n'est plus en contact avec un aimant
        {
            //on avise le reste du script
            dejaEnContact = false;
        }

        //delaiArret -= Time.deltaTime;

        //if (sensorValue == 0)
        //{
            //mouvementPedale = true;
        //    delaiArret = 2.5f;
       // }

       // if (delaiArret <= 0)
        //{
        //    Debug.Log("Le joueur ne fait plus tourner la roue");
            //mouvementPedale = false;
       // }

     


    }
}
