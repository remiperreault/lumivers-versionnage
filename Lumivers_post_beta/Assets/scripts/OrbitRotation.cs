﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitRotation : MonoBehaviour
{

    public int rotX;
    public int rotY;
    public int rotZ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //rotation de l'item sur lui-même
        transform.Rotate(new Vector3(rotX, rotY, rotZ) * Time.deltaTime);
    }
}
