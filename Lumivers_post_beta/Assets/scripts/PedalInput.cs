﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Uduino;

public class PedalInput : MonoBehaviour
{

    //*********************1h30 in********************
    public int sensorValue;
    public bool dejaEnContact = false;
    public float tempsSansMouvement = 10;//pour que le vaisseau soit immobile au debut du jeu

    // Start is called before the first frame update
    void Start()
    {
        //référence le capteur magnétique du arguino à un UrduinoManager (connextion et interaction à Unity)
        UduinoManager.Instance.pinMode(2, PinMode.Input_pullup);
    }

    // Update is called once per frame
    void Update()
    {
        //enregistrer la valeur du capteur dans une variable (0 ou 1)
        sensorValue = UduinoManager.Instance.digitalRead(2);

        //compteur du delai entre le passage de deux aimants
        tempsSansMouvement += Time.deltaTime;


        //si un aimant est détecté par le senseur (si la valeur passe à 0)
        if (sensorValue == 0) {

            if (!dejaEnContact)//si la roue n'est pas simplement immobile, un aimant sur le capteur
            {

                //reset float tempsSansMouvement/*********************************
                tempsSansMouvement = 0;

                /* on indique qu'un contact a été fait, pour s'assurer que le calcul de l'intervalle ne prenne en compte
                le passage d'un aimant une seule fois (et non a chaque frame que l'aimant est au-dessus du sensor)  */
                dejaEnContact = true;

            }
        }

        else if (sensorValue == 1)//si le capteur magnétique n'est plus en contact avec un aimant
        {
            //on avise le reste du script
            dejaEnContact = false;
        }

    }
}
