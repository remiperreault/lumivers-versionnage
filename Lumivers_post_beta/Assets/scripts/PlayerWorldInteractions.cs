﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerWorldInteractions : MonoBehaviour
{

    Scene currentScene;

    [Header("Textes fragments monde1")]
    public Text pickedUpCodesFragment1monde1Text;
    public Text pickedUpCodesFragment2monde1Text;
    public Text pickedUpCodesFragment3monde1Text;

    [Header("Textes fragments monde2")]
    public Text pickedUpCodesFragment1monde2Text;
    public Text pickedUpCodesFragment2monde2Text;
    public Text pickedUpCodesFragment3monde2Text;

    [Header("Textes fragments monde3")]
    public Text pickedUpCodesFragment1monde3Text;
    public Text pickedUpCodesFragment2monde3Text;
    public Text pickedUpCodesFragment3monde3Text;

    //public int pickedUpCodes;
    //public string pickedUpCodes;
    [Header("Fragments monde1")]
    static public string pickedUpCodeFragment1monde1;
    static public string pickedUpCodeFragment2monde1;
    static public string pickedUpCodeFragment3monde1;

    [Header("Fragments monde2")]
    static public string pickedUpCodeFragment1monde2;
    static public string pickedUpCodeFragment2monde2;
    static public string pickedUpCodeFragment3monde2;

    [Header("Fragments monde3")]
    static public string pickedUpCodeFragment1monde3;
    static public string pickedUpCodeFragment2monde3;
    static public string pickedUpCodeFragment3monde3;

    public int currentPickedUpCodeFragmentOrder;
    public int currentPicketUpCodeFragmentValue;

    AudioSource coinAudio;



    // Start is called before the first frame update
    void Start()
    {

        //on garde en mémoire quelle scène est active
        currentScene = SceneManager.GetActiveScene();

        coinAudio = GetComponent<AudioSource>();

        //affiche les fragments de codes déja trouvé par le joueur dans le UI
        //si le joueur a déja obtenu les fragments du code pour le monde 2 (qui sont dans le monde 1)

        //si fragment1 monde 1 actif
        if (pickedUpCodeFragment1monde1 != null && pickedUpCodeFragment1monde1 != "")
        {
            //affiche le fragment 1 menant au monde 2 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment1monde1);
            pickedUpCodesFragment1monde1Text.text = pickedUpCodeFragment1monde1;
        }
        //si fragment2 monde 1 actif
        if (pickedUpCodeFragment2monde1 != null && pickedUpCodeFragment2monde1 != "")
        {
            //affiche le fragment 2 menant au monde 2 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment2monde1);
            pickedUpCodesFragment2monde1Text.text = pickedUpCodeFragment2monde1;
        }
        //si fragment3 monde 1 actif
        if (pickedUpCodeFragment3monde1 != null && pickedUpCodeFragment3monde1 != "")
        {
            //affiche le fragment 3 menant au monde 2 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment3monde1);
            pickedUpCodesFragment3monde1Text.text = pickedUpCodeFragment3monde1;
        }



        //si fragment1 monde 2 actif
        if (pickedUpCodeFragment1monde2 != null && pickedUpCodeFragment1monde2 != "")
        {
            //affiche le fragment 1 menant au monde 3 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment1monde2);
            pickedUpCodesFragment1monde2Text.text = pickedUpCodeFragment1monde2;
        }
        //si fragment2 monde 2 actif
        if (pickedUpCodeFragment2monde2 != null && pickedUpCodeFragment2monde2 != "")
        {
            //affiche le fragment 2 menant au monde 3 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment2monde2);
            pickedUpCodesFragment2monde2Text.text = pickedUpCodeFragment2monde2;
        }
        //si fragment3 monde 2 actif
        if (pickedUpCodeFragment3monde2 != null && pickedUpCodeFragment3monde2 != "")
        {
            //affiche le fragment 3 menant au monde 3 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment3monde2);
            pickedUpCodesFragment3monde2Text.text = pickedUpCodeFragment3monde2;
        }



        //si fragment1 monde 3 actif
        if (pickedUpCodeFragment1monde3 != null && pickedUpCodeFragment1monde3 != "")
        {
            //affiche le fragment 1 menant au monde 1 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment1monde3);
            pickedUpCodesFragment1monde3Text.text = pickedUpCodeFragment1monde3;
        }
        //si fragment2 monde 3 actif
        if (pickedUpCodeFragment2monde3 != null && pickedUpCodeFragment2monde3 != "")
        {
            //affiche le fragment 2 menant au monde 1 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment2monde3);
            pickedUpCodesFragment2monde3Text.text = pickedUpCodeFragment2monde3;
        }
        //si fragment3 monde 3 actif
        if (pickedUpCodeFragment3monde3 != null && pickedUpCodeFragment3monde3 != "")
        {
            //affiche le fragment 3 menant au monde 1 dans le UI
            Debug.Log("le joueur a deja le code" + pickedUpCodeFragment3monde3);
            pickedUpCodesFragment3monde3Text.text = pickedUpCodeFragment3monde3;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //récolte et destruction des pièces
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            //enregistrer la valeur (le bout de code) ramassé par le joueur
            currentPickedUpCodeFragmentOrder = other.gameObject.GetComponent<PickupValue>().codeFragmentOrder;

            currentPicketUpCodeFragmentValue = other.gameObject.GetComponent<PickupValue>().teleportationCodePart;

            //on vérifie la position (l'ordre) du fragment trouvé dans la combinaison
            if (currentPickedUpCodeFragmentOrder == 1)
            {
                if (currentScene.name == "monde1")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment1monde1 == null || pickedUpCodeFragment1monde1 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment1monde1 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment1monde1Text.text = pickedUpCodeFragment1monde1;
                    }

                }

                else if (currentScene.name == "monde2")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment1monde2 == null || pickedUpCodeFragment1monde2 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment1monde2 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment1monde2Text.text = pickedUpCodeFragment1monde2;
                    }

                }

                else if (currentScene.name == "monde3")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment1monde3 == null || pickedUpCodeFragment1monde3 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment1monde3 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment1monde3Text.text = pickedUpCodeFragment1monde3;
                    }

                }

                Debug.Log("gained the fragment1");
            }

            if (currentPickedUpCodeFragmentOrder == 2)
            {

                if (currentScene.name == "monde1")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment2monde1 == null || pickedUpCodeFragment2monde1 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment2monde1 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment2monde1Text.text = pickedUpCodeFragment2monde1;
                    }

                }

                else if (currentScene.name == "monde2")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment2monde2 == null || pickedUpCodeFragment2monde2 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment2monde2 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment2monde2Text.text = pickedUpCodeFragment2monde2;
                    }

                }

                else if (currentScene.name == "monde3")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment2monde3 == null || pickedUpCodeFragment2monde3 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment2monde3 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment2monde3Text.text = pickedUpCodeFragment2monde3;
                    }

                }

                Debug.Log("gained the fragment2");
            }

            if (currentPickedUpCodeFragmentOrder == 3)
            {
                if (currentScene.name == "monde1")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment3monde1 == null || pickedUpCodeFragment3monde1 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment3monde1 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment3monde1Text.text = pickedUpCodeFragment3monde1;
                    }

                }

                else if (currentScene.name == "monde2")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment3monde2 == null || pickedUpCodeFragment3monde2 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment3monde2 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment3monde2Text.text = pickedUpCodeFragment3monde2;
                    }

                }

                else if (currentScene.name == "monde3")
                {
                    //si le joueur n'a pas déja collecté cette pièce
                    if (pickedUpCodeFragment3monde3 == null || pickedUpCodeFragment3monde3 == "")
                    {
                        //update le texte affichant le fragment de code trouvé
                        pickedUpCodeFragment3monde3 += currentPicketUpCodeFragmentValue;
                        pickedUpCodesFragment3monde3Text.text = pickedUpCodeFragment3monde3;
                    }

                }

                Debug.Log("gained the fragment3");
            }

            //on joue le son de collection d'un fragment de code
            coinAudio.Play();


            //on detruit la piece collectée par le joueur
            Destroy(other.gameObject);

        }
    }

    public void ResetCollectedCodeFragments()
    {
        //on vide les variables stockant les fragments de codes collectés = les champs de texte seront a nouveau vide
        pickedUpCodeFragment1monde1 = "";
        pickedUpCodeFragment2monde1 = "";
        pickedUpCodeFragment3monde1 = "";

        pickedUpCodeFragment1monde2 = "";
        pickedUpCodeFragment2monde2 = "";
        pickedUpCodeFragment3monde2 = "";

        pickedUpCodeFragment1monde3 = "";
        pickedUpCodeFragment2monde3 = "";
        pickedUpCodeFragment3monde3 = "";
    }
}
