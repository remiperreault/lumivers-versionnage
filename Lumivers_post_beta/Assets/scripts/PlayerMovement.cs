﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //le cube bouge seulement si on touche a un aimant en moins de 1.5s

    public GameObject arduino;

    public Rigidbody rb;
    //public float arduino_delai;
    public float arduino_tempsEntreAimants;

    public float speed = 30f;
    public float tempsRotationMax = 1.5f;//temps maximum acepté pour passer entre deux aimants sur la roue et faire avancer le vaisseau. Équivalent a la vitesse de rotation minimale

    public float inHoriz;
    public float inVert;
   



    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();


        //activation des controles permettant au joueur de regarder autour de lui et d'avancer
        //playerCanMove = true;


    }

    void Update()
    {

        //rotation du vaisseau
        inHoriz = Input.GetAxis("Horizontal")/2;
        inVert = Input.GetAxis("Vertical")/2;



        
        //rotation de l'objet sur lequel le script est attache, vecteur3 (x, y, z) / x est la ratation haut/bas et z est la rotation gauche/droite
        transform.Rotate(inVert, inHoriz, -inHoriz);
      
        //transform.Rotate(-Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), 0.0f);



    }

    // Update is called once per frame
    void FixedUpdate()//on utilise fixedUpdate pour pouvoir utiliser les valeurs calculées dans la fonction update d'un autre script pendant le même frame
    {

        arduino_tempsEntreAimants = arduino.GetComponent<PedalInput>().tempsSansMouvement; // on récupère la variable `tempsEntreAimants` du script sur l'Arduino et on assigne sa valeur à une nouvelle variable.







        //si on capte un nouvel aimant avant le delai imparti
        //if (arduino_tempsEntreAimants < 1.5f)
        //if (arduino_tempsEntreAimants < 2.0f) 
        if (arduino_tempsEntreAimants < 0.5f || Input.GetKey("space")) //fix when arduino doesn't work, press space instead of rolling
        {
            //Vector3 DirectionVect = new Vector3(0, 0, speed * Time.fixedDeltaTime);
            //tempVect = tempVect.normalized * speed * Time.deltaTime;
            //rb.MovePosition(transform.position + DirectionVect);

            transform.position += transform.forward * speed * Time.fixedDeltaTime;
        }
        else
        {
            //dont move
        }




    }
}
